<?php
include_once 'vdiskinit.php';
header('Content-Type: text/plain; charset=utf-8');
if($vdiskLogedIn){
	if(($tokenObj->expires_in - time())<VDISK_TOKEN_UPDATEOFFSET){
		try{
			$newToken=$oauth2->getAccessToken('token', array('refresh_token'=>$tokenObj->refresh_token,'redirect_uri'=>VDISK_CALLBACK_URL));
			saveToken($newToken);
			echo 'Token refreshed.';
		}catch(Exception $e){
			echo 'Error:'.$e->getMessage();
		}
	}else{
		echo 'No need to update token.';
	}
}else{
	echo 'Error: No old token found. Please auth this app first.';
}
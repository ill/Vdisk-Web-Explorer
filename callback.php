<?php
header('Content-Type: text/plain; charset=UTF-8');
include_once 'vdiskinit.php';
session_start();
if(!isset($_SESSION['admin']) or !$_SESSION['admin']){
	header('Location: options.php?command=login');
	exit;
}
if(isset($_REQUEST['code'])){
	$keys = array();
	$keys['code']=$_REQUEST['code'];
	$keys['redirect_uri']=VDISK_CALLBACK_URL;
	try{
		$token=$oauth2->getAccessToken('code', $keys);
		saveToken($token);
		header('Location: options.php?command=list');
	}catch(Exception $e){
		print_r($e->getMessage());
	}
}
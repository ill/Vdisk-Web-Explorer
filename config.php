<?php
define('VDISK_APP_ADMINPSW', '12345');
define('VDISK_CLIENT_ID', '1387518917');
define('VDISK_CLIENT_SECRET', 'c7f5804b935f60035b48a569fced7f3a');
define('VDISK_CALLBACK_URL', 'http://t.x.gg/c.php/localhost:8080/vdisk/callback.php');
define('VDISK_TOKEN_SAVEPATH', 'token.json');
define('VDISK_AUTH_RIGHTS', 'sandbox');
define('VDISK_PUBLIC_DOWNLOAD', TRUE);
define('VDISK_PUBLIC_VIEW', TRUE);
define('VDISK_TOKEN_UPDATEOFFSET', 10800);

define('APPROOT',dirname(__FILE__));
function saveToken($tokenObject){
	file_put_contents(VDISK_TOKEN_SAVEPATH, json_encode($tokenObject));
}

function loadToken(){
	if(file_exists(VDISK_TOKEN_SAVEPATH)){
		return json_decode(file_get_contents(VDISK_TOKEN_SAVEPATH));
	}else{
		return NULL;
	}
}
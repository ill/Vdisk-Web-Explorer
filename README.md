(#) VdiskFileShare
============
请先确保您已经 [开通新浪微盘](http://vdisk.weibo.com/file/list?log_target=navigation_vdisk) 。

开通新浪微盘可能需要 [注册新浪微博用户](http://weibo.com/signup/signup.php?lang=zh-cn) 。

### 运行要求

* PHP 5.3.1 或以后版本
* PHP cURL 支持
* 能够与 auth.sina.com.cn、api.weipan.cn、upload-vdisk.sina.com.cn 建立网络连接。

### 使用说明
- 授权ID
```
您需要有一个可用的 CLIENT ID 和与之对应的 CLIENT SECRET 才可以使用本程序。


我们提供一个具有 Sandbox 权限公用的 CLIENT ID 和 CLIENT SECRET：
CLIENT ID：1387518917
CLIENT SECRET：c7f5804b935f60035b48a569fced7f3a

由于新浪限制，使用公共 Client ID ，请把你的回调地址添加在我的网址后面就像下面一样：
你的回调网址：http://www.example.com/vdiskshare/callback.php
当你使用公共 Client ID 时，你需要使用回调网址：http://t.x.gg/c.php/www.example.com/vdiskshare/callback.php

```
- 更改设置
```
编辑 config.php，按照已有的格式填写您的 CLIENT ID 和 CLIENT SECRET 以及回调地址 VDISK_CALLBACK_URL。
VDISK_APP_ADMINPSW 是应用的管理密码，在修改数据的时候需要提供此密码。
VDISK_CLIENT_ID 是您的应用 ID。
VDISK_CLIENT_SECRET 是您的应用 App Secret。
VDISK_CALLBACK_URL 是您的回调地址，应该指向您搭建的 Web 服务器。
VDISK_TOKEN_SAVEPATH 是获取到的 Token 的保存到文件地址，您应该将它改成一个不能被外部访问的地址。
VDISK_AUTH_RIGHTS 是应用接入授权的权限，默认为 sandbox 。
	sandbox：仅访问该应用的文件。
	basic：可访问微盘里面的其他文件。
VDISK_PUBLIC_DOWNLOAD 是控制未使用应用的管理密码登陆的用户的下载功能的。
	TRUE：允许所有人下载文件；
	FALSE：仅使用应用的管理密码登陆的人可以下载文件。
VDISK_PUBLIC_VIEW 是控制未使用应用的管理密码登陆的用户的列表文件功能的。 
	TRUE：允许所有人查看文件列表，但不能改动文件；
	FALSE：仅使用应用的管理密码登陆的人可以查看文件列表。
VDISK_TOKEN_UPDATEOFFSET 是 Token 更新时间与 Token 失效时间的最低差值，默认 7200 。
	更新周期会受到您的 Conjob 设定影响；
	值越大，更新越频繁，这个值原则上不能超过 86400 ；
	设定值太小可能会造成时而出现 Token 过期的现象，甚至会要求重新登录。
	新浪设置有单个应用单个用户一段时间内更新 Token 次数上限，此值设置不宜过大。
```
- 部署程序
```
上传文件到您的 Web 服务器上。
访问 auth.php 接入新浪的授权，需要管理密码才能执行此操作。
使用 conjob 执行 renewtoken.php 以避免 Token 过期。
若 Token 过期请删除 Token 储存文件再重新授权。
```
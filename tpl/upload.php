<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <title>Upload</title>
</head>
<body>
	Select a file upload to <?php echo $path;?>:
	<br />
	<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post" enctype="multipart/form-data">
		<input name="uploadFile" type="file" size="16" />
		<br />
		<input name="command" type="hidden" value="upload" />
		<input name="path" type="hidden" value="<?php echo $path;?>" />
		<input type="submit" value="Upload" />
	</form>
	<br />
	<a href="options.php?command=list&amp;path=<?php echo urlencode($ref);?>">Back</a>
</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta http-equiv="Content-Style-Type" content="text/css">
  <title>Make dir</title>
</head>
<body>
	Enter new directory name:
	<br />
	<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
		<input name="dirname" type="text" size="16" />
		<br />
		<input name="path" type="hidden" value="<?php echo $path;?>" />
		<input name="command" type="hidden" value="mkdir" />
		<input type="submit" value="Create" />
	</form>
	<br />
	<a href="options.php?command=list&amp;path=<?php echo urlencode($ref);?>">Back</a>
</body>
</html>